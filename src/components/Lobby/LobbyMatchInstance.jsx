/*
 * Copyright 2018 The boardgame.io Authors.
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import {
	Button,
	TableCell,
	TableRow,
	CircularProgress
} from '@material-ui/core';
import React, { useEffect, useState } from 'react';

const LobbyMatchInstance = (props) => {
	const [joiningMatch, setJoiningMatch] = useState(false);
	const [matchInstance, setMatchInstace] = useState(undefined);
	const [seatId, setSeatId] = useState('');

	const { errorMsg } = props;

	useEffect(() => {
		props.match && setMatchInstace(props.match);
	}, [props]);

	useEffect(() => {
		if (
			errorMsg &&
			matchInstance &&
			joiningMatch &&
			matchInstance.players.some((player) => !player.name) &&
			!matchInstance.players.find((player) => player.name === props.playerName)
		) {
			const freeSeat = matchInstance.players.find(
				(player) => !player.name && player.id !== seatId
			);

			if (freeSeat) {
				setSeatId(freeSeat.id);
				handleJoinMatch(matchInstance, freeSeat.id);
			} else {
				setJoiningMatch(false);
			}
		} else {
			setJoiningMatch(false);
		}
	}, [errorMsg, matchInstance]);

	const handleJoinMatch = (inst, seatId) => {
		props.onClickJoin(inst.gameName, inst.matchID, '' + seatId);
	};

	const _createSeat = (player) => {
		return player.name || '[free]';
	};

	const _createButtonJoin = (inst, seatId) => (
		<Button
			variant="contained"
			color="primary"
			disabled={joiningMatch}
			key={'button-join-' + inst.matchID}
			onClick={() => {
				setJoiningMatch(true);
				handleJoinMatch(inst, seatId);
			}}
		>
			{!joiningMatch && 'Join'}
			{joiningMatch && <CircularProgress size={16} />}
		</Button>
	);

	const _createButtonLeave = (inst) => (
		<Button
			variant="contained"
			color="secondary"
			key={'button-leave-' + inst.matchID}
			onClick={() => {
				setJoiningMatch(false);
				props.onClickLeave(inst.gameName, inst.matchID);
			}}
		>
			Leave
		</Button>
	);

	const _createButtonPlay = (inst, seatId) => (
		<Button
			variant="contained"
			color="primary"
			key={'button-play-' + inst.matchID}
			onClick={() =>
				props.onClickPlay(inst.gameName, {
					matchID: inst.matchID,
					playerID: '' + seatId,
					numPlayers: inst.players.length
				})
			}
		>
			Play
		</Button>
	);

	const _createButtonSpectate = (inst) => (
		<Button
			variant="contained"
			color="primary"
			key={'button-spectate-' + inst.matchID}
			onClick={() =>
				props.onClickPlay(inst.gameName, {
					matchID: inst.matchID,
					numPlayers: inst.players.length
				})
			}
		>
			Spectate
		</Button>
	);

	const _createInstanceButtons = (inst) => {
		const playerSeat = inst.players.find(
			(player) => player.name === props.playerName
		);
		const freeSeat = inst.players.find((player) => !player.name);
		if (playerSeat && freeSeat) {
			// already seated: waiting for match to start
			return _createButtonLeave(inst);
		}
		if (freeSeat) {
			// at least 1 seat is available
			return _createButtonJoin(inst, freeSeat.id);
		}
		// match is full
		if (playerSeat) {
			return (
				<div>
					{[_createButtonPlay(inst, playerSeat.id), _createButtonLeave(inst)]}
				</div>
			);
		}
		// allow spectating
		// return _createButtonSpectate(inst);
	};

	const match = props.match;
	let status = 'OPEN';
	if (!match.players.find((player) => !player.name)) {
		status = 'RUNNING';
	}
	return (
		<TableRow key={'line-' + match.matchID}>
			<TableCell key={'cell-name-' + match.matchID}>{match.gameName}</TableCell>
			<TableCell
				key={'cell-status-' + match.matchID}
				style={{ color: status === 'OPEN' ? 'green' : 'blue' }}
			>
				{status}
			</TableCell>
			<TableCell key={'cell-seats-' + match.matchID}>
				{match.players.map(_createSeat).join(', ')}
			</TableCell>
			<TableCell key={'cell-buttons-' + match.matchID} align="right">
				{_createInstanceButtons(match)}
			</TableCell>
		</TableRow>
	);
};

export default LobbyMatchInstance;
