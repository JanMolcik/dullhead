# RULES

Poohead is a card game, the object of which is to lose all of one's playing cards, with the final player being the "shithead".

### SETUP

At the beginning of the game, players are allowed to switch their hand cards with their face-up cards in an attempt to produce a strong set of face-up cards (possibly all perfect magic cards) for later in the game.

### GAMEPLAY

The beginning player is a person with 3 in their hand. If no player has a 3 in hand, the game is started with a 4; if no 4, then a 5, and so on. The second player must then place an equal or higher card (in numerical value) than the card played previously, this card is to be put on top of the play pile. All subsequent players are then to follow this rule. The player would then have to draw cards from the deck. Each player should have at least 3 cards in their hand at all times, unless the deck has run out of cards. The game continues sequentially in a clockwise direction unless certain magic cards are played.

If a player is able to place four cards with the same numerical value (e.g. 5♦ 5♣ 5♥ 5♠ or 8♥ 8♦ 8♠ 8♣), this burns the discards in the same manner as a ten. Burning can also happen across multiple players' turns: for example, if a player first plays 5♥ 5♦ 5♠ and the next player in turn has the 5♣, they can drop that card to finish the set and burn the play pile. The player who burns the pile must then play another card after.

When a player has no magic cards and no single card that is equal or higher in value than the card on top of the play pile, they must pick up all the cards on the play pile and end their turn. Picking up the pile can often put a player at a great disadvantage when many cards have been played, as they will have more cards to shed than other players. Even so, it is still possible to quickly recover from this handicap by burning the pile.

After a player has no more cards in their hand, and the deck is empty, they need to play from their three face-up cards. They cannot play from this set of cards until they have finished with their hand. Following the rule: the value of the face-up card must be higher than the value of the card on the top of the pile, if a player cannot play the face-up card, then they must pick up the pile (with the lowest value face-up card). Once all of the face-up cards have been played, a player must then play their blind cards. These cards are played one at a time, without the player knowing the card until the moment it is played. As usual, if the chosen card is lower than the previous card played, they need to pick up the pile, and are required to play their entire hand again before progressing to the rest of their face-down cards.

After a player has no cards left, they are out. The game progresses until only one player is left. The final player left in the game is known as the "shithead". Under most rules, the shithead's only role is to deal the next set of cards. Players must determine a punishment for being the "shithead", such as the shithead must fetch the next round of drinks or do something humiliating.

### MAGIC CARDS

Magic cards can be played on any card and trigger special effect changing the game flow.

| Card | Label | Effect |
| ------ | ------ | ------ |
| **2** | reset pile | can be played on anything |
| **8** | invisible | next player has to beat card below this one |
| **10** | burn pile | "burn" discard pile, player play again |
| **J** | swap order | same as **8** and swap turn order |

### SEMI-MAGIC CARD

This card unlike magic cards **can't** be played on any higher value card but has special effect.

| Card | Label | Effect |
| ------ | ------ | ------ |
| **7** | seven or lower | next player must play 7 or lower value card |
