import { Server } from 'boardgame.io/server';
import { Poohead } from './Poohead';
import path from 'path';
import serve from 'koa-static';

const server = Server({ games: [Poohead] });
const PORT = process.env.PORT || 8000;

const frontEndAppBuildPath = path.resolve(__dirname, '../../build');

if (server.app.env === 'production') {
	server.app.use(serve(frontEndAppBuildPath));

	server.run(PORT, () => {
		server.app.use(
			async (ctx, next) =>
				await serve(frontEndAppBuildPath)(
					Object.assign(ctx, { path: 'index.html' }),
					next
				)
		);
	});
} else {
	server.run({ port: PORT, lobbyConfig: { apiPort: 8080 } });
}
