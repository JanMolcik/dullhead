import {
	InitPlayers,
	GenerateDeck,
	SwapCards,
	PlayHandCards,
	PlayBoardCard,
	PlayUnknownCards,
	DrawThenPlay,
	TakePile,
	canPlay,
	updateFinishedPlayers
} from './helpers';

/**
 *
 *
 */

// Test of card swap (hand and board, board and hand)
it('should swap selected card from hand and selected card from board cards', () => {
	// Simplified Game object
	const G = {
		players: {
			0: {
				hand: ['AC0', '8D0', 'KD1'],
				knownCards: ['7H0', '3C1', '8S0']
			}
		}
	};

	// Simplified context object
	const ctx = {
		playerID: '0'
	};

	const handCard1ID = 'AC0';
	const boardCard1ID = '3C1';

	// Swap hand and board cards
	SwapCards(G, ctx, handCard1ID, boardCard1ID);

	const handCard2ID = '8D0';
	const boardCard2ID = '7H0';

	// Swap board and hand card
	SwapCards(G, ctx, boardCard2ID, handCard2ID);

	expect(G).toEqual({
		players: {
			0: {
				hand: ['3C1', '7H0', 'KD1'],
				knownCards: ['8D0', 'AC0', '8S0']
			}
		}
	});
});

it('should play cards from hand', () => {
	const G = {};

	expect(G).toEqual({});
});
