module.exports = {
	purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
	darkMode: false, // or 'media' or 'class'
	theme: {
		boxShadow: {
			placeholder: '0 0 0 1px #f8f9fa',
			highlighted: '0 0 0 3px #F4D03F'
		},
		borderRadius: {
			2: '2px'
		},
		colors: {
			white: '#f8f9fa',
			grey: '#cfcfcf'
		},
		maxWidth: {
			unset: 'unset',
			55: '55px',
			'1/4': '25%',
			'1/2': '50%',
			'3/4': '75%'
		},
		extend: {
			translate: {
				180: '180deg'
			},
			spacing: {
				'-15': '-15px',
				'-30': '-30px'
			},
			gridTemplateColumns: {
				cards: 'repeat(auto-fit, minmax(12px, max-content))'
			},
			gridTemplateRows: {
				'center-board': 'repeat(3, minmax(0, max-content))',
				cards: 'repeat(auto-fill, minmax(20px, max-content))'
			},
			gridAutoRows: {
				cards: '20px'
			},
			padding: {
				unset: 'unset',
				35: '35px'
			},
			margin: {
				'-5': '-5px',
				5: '5px',
				20: '20px'
			},
			gap: {
				5: '5px'
			},
			animation: {
				'pulse-player': 'pulse-player 2s ease-out 2',
				appear: 'appear 1.5s',
				'rotate-cw': 'rotate-cw 20s linear infinite',
				'rotate-ccw': 'rotate-ccw 20s linear infinite'
			},
			keyframes: {
				'pulse-player': {
					'0%': { 'box-shadow': '0 0 0 6px rgba(255, 230, 109, 1)' },
					'25%': { 'box-shadow': '0 0 0 14px rgba(255, 230, 109, 0.75)' },
					'100%': { 'box-shadow': '0 0 0 6px rgba(255, 230, 109, 1)' }
				},
				appear: {
					from: {
						opacity: '0'
					},
					to: {
						opacity: '1'
					}
				},
				'rotate-cw': {
					from: { transform: 'rotate(0deg)' },
					to: { transform: 'rotate(360deg)' }
				},
				'rotate-ccw': {
					from: { transform: 'rotate(0deg)' },
					to: { transform: 'rotate(-360deg)' }
				}
			}
		}
	},
	variants: {
		extend: {}
	},
	plugins: []
};
